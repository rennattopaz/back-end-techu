# Imagen docker base inicial
FROM node:latest

#Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-api

#Copiar archivos del proyecto en directorio de Docker
ADD . /docker-api

#Instalar las dependencias del proyecto en produccion
# RUN npm install --production
# RUN npm install --only=production

#Puerto donde exponemos el contenedor (mismo que definimos en nuestra API)
EXPOSE 3000

#Lanzar la aplicacion (appemlab.js)
CMD ["npm","start"]

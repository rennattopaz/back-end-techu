var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';
app.use(bodyParser.json());

//GET users
app.get(URLbase + 'users',
 function(req, res){
   console.log('GET /apiperu/v1/users');
   res.send(userFile);
   //res.status(200).send({"msg": "respuesta correcta"});
 });
//GET users por ID
/*app.get(URLbase + 'users/:id',
function(req,res){
  console.log('GET /apiperu/v1/users/:id');
  console.log(req.params);
  console.log(req.params.id);
  let idUsr = req.params.id;
  let userData = userFile[idUsr-1];
  if(userData == null || typeof userData == "undefined"){
    userData={"msg":"0"};
  }
  res.send(userData);
});*/

//GET users por ID
app.get(URLbase + 'users/:id/:id2',
function(req,res){
  console.log('GET /apiperu/v1/users/:id/:id2');
  console.log(req.params);
  console.log(req.query);
  res.send({"msg":"0"});
});

//GET users con Query string
app.get(URLbase + 'users2',
function(req,res){
  console.log(req.query);
  console.log(req.query.id);
  let idUsr = req.query.id;
  let userData = userFile[idUsr-1];
  if(userData == null || typeof userData == "undefined"){
    userData={"msg":"0"};
  }
  res.send(userData);
});

//POST users
app.post(URLbase + 'users',
function(req,res){
    console.log('POST /apiperu/v1/users');
    let newID = userFile.length + 1;
    let newUser = {
      "userId":newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    userFile.push(newUser);
    //console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario añadido correctamente ", newUser});
});

//PUT user
app.put(URLbase + 'users/:id',
function(req,res){
    console.log('PUT /apiperu/v1/users/:id');
    let id = req.params.id;
    let userID = id-1;
    let userData = userFile[userID];
    if(userData == null || typeof userData == "undefined"){
        console.log("ERROR");
      res.status(404);
      res.send({"msg":"No encontrado"});
    } else{
      console.log(userFile[userID]);
      userFile[userID].first_name=req.body.first_name;
      userFile[userID].last_name=req.body.last_name;
      userFile[userID].email=req.body.email;
      userFile[userID].password=req.body.password;
      console.log(userFile[userID]);
      res.status(201);
      res.send({"msg":"Usuario modificado correctamente "});
    }
});

//DELETE user
app.delete(URLbase + 'users/:id',
function(req,res){
    console.log('DELETE /apiperu/v1/users/:id');
    let id = req.params.id;
    let userID = id-1;
    let userData = userFile[userID];
    if(userData == null || typeof userData == "undefined"){
        console.log("ERROR");
      res.status(404);
      res.send({"msg":"No encontrado"});
    }
    else{
    console.log(userFile[userID]);
    //userFile.splice(userID);
    userFile.splice(userID,1);
    console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario Eliminado correctamente "});
  }
});


// app.get('/', function (req, res) {
//   res.send('Hola Lima Peru!');
// });
//app.listen(3000);
app.listen(port, function () {
 console.log('Node escucha en puerto ' + port);
});

var express = require('express');
var userFile = require('./user.json');
var requestJSON = require('request-json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase   = '/apiperu/v1/';
var URLbaseV5 = '/apiperu/v5/';


var urlMlabRaiz='https://api.mlab.com/api/1/databases/apiperudb/collections/';
var apiKeyLab ="apiKey=UGmbKwey0b12C9gR3qaUtXEzT7garMCI";

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());

app.get(URLbase + 'user',
function(req,res){
var httpClient = requestJSON.createClient(urlMlabRaiz);
//console.log("Cliente http mlab creado.");

var queryString = 'f={"_id":0}';
//console.log(urlMlabRaiz + 'user/?' + queryString + '&'+ apiKeyLab);
httpClient.get('user/?' + queryString + '&'+ apiKeyLab,
  function(err,respuestaMlab,body){
//    console.log(body);
    var response = {};
    if(err){
      response = {"msg":"Error obteniendo usuario"}
      res.status(500);
    }
    else{
      if(body.length > 0){
        response = body;
      }
      else{
        response = {"msg":"Ningun elemento 'user'."}
        res.status(404);
      }
    }
    res.send(response);
  });
});


//GET users
/*app.get(URLbase + 'users',
 function(req, res){
   console.log('GET /apiperu/v1/users');
   res.send(userFile);
   //res.status(200).send({"msg": "respuesta correcta"});
 });*/
//GET users por ID
app.put(URLbaseV5 + 'users/:id',
function(req,res){
  var id = req.params.id;
  var actualizar = req.body;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = requestJSON.createClient(urlMlabRaiz);
  clienteMlab.get('user?'+ queryStringID + "&l=1&" + apiKeyLab,
    function(error, respuestaMLab , body) {
      if (!error) {
          if (body.length >0){
            var datos =
                '{"first_name":"' + actualizar.first_name+'",'+
                '"last_name_1":"' + actualizar.last_name_1+'",'+
                '"last_name_2":"' + actualizar.last_name_2+'",'+
                '"job_title":"' + actualizar.job_title+'",'+
                '"email":"' + actualizar.email +'"}';

            //console.log(datos);
             var cambio = '{"$set":'+datos+'}';
             //console.log(cambio);

             clienteMlab.put('user?' + 'q={"id": ' + body[0].id + '}&' + apiKeyLab, JSON.parse(cambio),
             function(errP, resP, bodyP){
               if(!errP){
                 //console.log("ok update");
                 res.send({"login":"ok", "resp":bodyP[0]});
               }
             })
           }
           else {
             res.status(404).send('Usuario no encontrado');
           }
      }
      else {
        res.status(404).send('Usuario no encontrado');
      }
    });
});

app.get(URLbaseV5 +'accounts',
 function(req, res) {
   //console.log(req);
   var userId = req.query.userid;
  // console.log("get accounts");
   //var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   var queryStringID = 'q={"client_id":' + userId + '}';
  // console.log(queryStringID);
   clienteMlab = requestJSON.createClient(urlMlabRaiz);
//   console.log(urlMlabRaiz + '/account?' + queryStringID + "&" + apiKeyLab);
   clienteMlab.get('account?' + queryStringID + "&" + apiKeyLab,
   function(err, resM, body) {
     var response = {};
     //console.log("GET ACCOUNTS ");
      //console.log(body);
    if (!err) {
        if(body.length > 0){ // Login ok
            //console.log("GET ACCOUNTS - LIST " + body.length);
              response = body;
         }
         else {
           response = {"msg":"Ningun elemento para el usuarios."}
           res.status(404);
         }
    }
    else {
      response = {"msg":"No se encontro informacion del usuario."}
      res.status(404);
    }
    res.send(response);
  })
});

app.get(URLbaseV5 +'movements',
 function(req, res) {
   //console.log(req);
   var accountId = req.query.accountid;
   //console.log("get movements");
   //var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   var queryStringID = 'q={"account_number":' + accountId + '}';
   //console.log(queryStringID);
   clienteMlab = requestJSON.createClient(urlMlabRaiz);
   //console.log(urlMlabRaiz + '/movement?' + queryStringID + "&" + apiKeyLab);
   clienteMlab.get('movement?' + queryStringID + "&" + apiKeyLab,
   function(err, resM, body) {
     var response = {};
    // console.log("GET MOVEMENTS ");
      //console.log(body);
    if (!err) {
        if(body.length > 0){ // Login ok
            //console.log("GET MOVEMENTS - LIST " + body.length);
              response = body;
         }
         else {
           response = {"msg":"Ningun movmiento para esta cuenta."}
           res.status(404);
         }
    }
    else {
      response = {"msg":"No se informacion para el usuario."}
      res.status(404);
    }
    res.send(response);
  })
});

app.get(URLbaseV5 +'userinfo',
 function(req, res) {
   //console.log(req);
   var userid = req.query.userid;
   var queryStringID = 'q={"id":' + userid + '}';
   //console.log(queryStringID);
   clienteMlab = requestJSON.createClient(urlMlabRaiz);
   //console.log(urlMlabRaiz + '/movement?' + queryStringID + "&" + apiKeyLab);
   clienteMlab.get('user?' + queryStringID + "&l=1&" +apiKeyLab,
   function(err, resM, body) {
     var response = {};
    if (!err) {
        if(body.length > 0){
              response = body;
         }
         else {
           response = {"msg":"No se encontro info del usuario."}
           res.status(404);
         }
    }
    else {
      response = {"msg":"No se informacion para el usuario."}
      res.status(404);
    }
    res.send(response);
  })
});

app.post(URLbaseV5 +'login',
 function(req, res) {
  // console.log("LOGIN");
   var email = req.body.email;
   var password = req.body.password;

   var query = 'q={"email":"' + email + '","password":"' + password + '"}'

   clienteMlab = requestJSON.createClient(urlMlabRaiz);
   //console.log(urlMlabRaiz + '/user?' + query + "&l=1&" + apiKeyLab);
   clienteMlab.get('user?' + query + "&l=1&" + apiKeyLab,
   function(err, resM, body) {

      //console.log(body);
    if (!err) {

        if (body.length >0){ // Login ok
           var cambio = '{"$set":{"logged":true}}';
          // console.log(urlMlabRaiz + 'user?' + 'q={"id": ' + body[0].id + '}&' + apiKeyLab);
           clienteMlab.put('user?' + 'q={"id": ' + body[0].id + '}&' + apiKeyLab, JSON.parse(cambio),
           function(errP, resP, bodyP){
             res.send({"login":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name_1});
           })
         }
         else {
           res.status(404).send('Usuario no encontrado');
         }
    }
    else {
      res.status(404).send('Usuario no encontrado');
    }
  })
});

app.post(URLbaseV5 +'logout',
  function(req, res) {
    //console.log("LOGOUT");
    var id = req.body.id
    var query = 'q={"id":' + id + ', "logged":true}';
    clienteMlab = requestJSON.createClient(urlMlabRaiz);
    //console.log(urlMlabRaiz + 'user?' + query + "&l=1&" + apiKeyLab);
    clienteMlab.get('user?' + query + "&l=1&" + apiKeyLab,
      function(err, resM, body) {
        if (!err) {
          if (body.length == 1){
            //console.log("ENCONTRADO");
            var cambio = '{"$set":{"logged":false}}'
            clienteMlab.put('user?' + 'q={"id": ' + body[0].id + '}&' + apiKeyLab, JSON.parse(cambio),
              function(errP, resP, bodyP) {
                //console.log("Logout ok");
                res.send({"logout":"ok", "id":body[0].id})
              });
          }
          else {res.status(200).send('Usuario no logado previamente');}
        }
      });
});
//GET users por ID
/*app.get(URLbase + 'users/:id/:id2',
function(req,res){
  console.log('GET /apiperu/v1/users/:id/:id2');
  console.log(req.params);
  console.log(req.query);
  res.send({"msg":"0"});
});

//GET users con Query string
app.get(URLbase + 'users2',
function(req,res){
  console.log(req.query);
  console.log(req.query.id);
  let idUsr = req.query.id;
  let userData = userFile[idUsr-1];
  if(userData == null || typeof userData == "undefined"){
    userData={"msg":"0"};
  }
  res.send(userData);
});*/

//POST users
/*app.post(URLbase + 'users',
function(req,res){
    console.log('POST /apiperu/v1/users');
    let newID = userFile.length + 1;
    let newUser = {
      "userId":newID,
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password":req.body.password
    }
    userFile.push(newUser);
    //console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario añadido correctamente ", newUser});
});*/

//PUT of user
/*app.put(URLbase + 'users/:id',
function(req, res) {
  console.log("PUT");
  var id = req.params.id;
  console.log("ID " + id);
  var queryStringID = 'q={"userID":' + id + '}&';
  var clienteMlab = requestJSON.createClient(urlMlabRaiz);
//  'user/?' + queryString + '&'+ apiKeyLab,
console.log(urlMlabRaiz + 'user?'+ queryStringID + apiKeyLab);
  clienteMlab.get('user?'+ queryStringID + apiKeyLab ,
    function(error, respuestaMLab , body) {
      console.log("PUT CLiente");
      console.log(req.body);
      console.log(body);
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     clienteMlab.put(urlMlabRaiz + 'user?q={"userID": ' + id + '}&' + apiKeyLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log(body);
       res.send(body);
      });
 });
});*/

// Petición PUT con id de mLab (_id.$oid)
  app.put(URLbase + 'usersmLab/:id',
    function (req, res) {
      var id = req.params.id;
      let userBody = req.body;
      var queryString = 'q={"userID":' + id + '}&';
      var httpClient = requestJSON.createClient(urlMlabRaiz);
      //console.log(urlMlabRaiz + 'user?' + queryString + apiKeyLab);
      httpClient.get('user?' + queryString + apiKeyLab,
        function(err, respuestaMLab, body){
          let response = body;
          //console.log(body);
          //$oid
          //Actualizo los campos del usuario
          let updatedUser = {
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : req.body.password
          };//Otra forma simplificada (para muchas propiedades)
          // var updatedUser = {};
          // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         var cambio = '{"$set":' + JSON.stringify(updatedUser) + '}';
          //Llamo al put de mlab.
          httpClient.put('user/' + response[0]._id.$oid + '?' + apiKeyLab,  JSON.parse(cambio),
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg" : "Error actualizando usuario."
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg" : "Usuario actualizado correctamente."
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });


//POST of user
app.post(URLbase + 'users',
function(req, res) {
  //console.log("POST");
  var clienteMlab = requestJSON.createClient(urlMlabRaiz);

  let newUser = {
    "userId":req.body.userid,
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password
  }
  //console.log(newUser);
  //console.log(JSON.parse(newUser));
  clienteMlab.post('user?'+ '&' + apiKeyLab , newUser,
    function(error, respuestaMLab , body) {
        //console.log(body);
        res.send(body);
      });
 });


//DELETE user with id
app.delete(URLbase + "users/:id",
  function(req, res){
    //console.log("entra al DELETE");
    //console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    //console.log(urlMlabRaiz + 'user?' + queryStringID + apiKeyLab);
    var httpClient = requestJSON.createClient(urlMlabRaiz);
    httpClient.get('user?' +  queryStringID + apiKeyLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        //console.log("body delete:"+ respuesta);
        httpClient.delete(urlMlabRaiz + "user/" + respuesta._id.$oid +'?'+ apiKeyLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });
/*app.put(URLbase + 'users/:id',
function(req,res){
    console.log('PUT /apiperu/v1/users/:id');
    let id = req.params.id;
    let userID = id-1;
    let userData = userFile[userID];
    if(userData == null || typeof userData == "undefined"){
        console.log("ERROR");
      res.status(404);
      res.send({"msg":"No encontrado"});
    } else{
      console.log(userFile[userID]);
      userFile[userID].first_name=req.body.first_name;
      userFile[userID].last_name=req.body.last_name;
      userFile[userID].email=req.body.email;
      userFile[userID].password=req.body.password;
      console.log(userFile[userID]);
      res.status(201);
      res.send({"msg":"Usuario modificado correctamente "});
    }
});*/

//DELETE user
app.delete(URLbase + 'users/:id',
function(req,res){
    //console.log('DELETE /apiperu/v1/users/:id');
    let id = req.params.id;
    let userID = id-1;
    let userData = userFile[userID];
    if(userData == null || typeof userData == "undefined"){
        //console.log("ERROR");
      res.status(404);
      res.send({"msg":"No encontrado"});
    }
    else{
    //console.log(userFile[userID]);
    //userFile.splice(userID);
    userFile.splice(userID,1);
    //console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario Eliminado correctamente "});
  }
});


// app.get('/', function (req, res) {
//   res.send('Hola Lima Peru!');
// });
//app.listen(3000);
app.listen(port, function () {
 console.log('Node escucha en puerto ' + port);
});
